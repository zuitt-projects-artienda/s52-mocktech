function countLetter(letter, sentence) {
    let count = 0

    if (letter.length !== 1) {

        return undefined;

    } else {

        for (let i = 0;  i < sentence.length; i++) {

            if (sentence[i] === letter) {
                count += 1;

            } else {
                continue
            }
        }

        return count;
    }
   
}


function isIsogram(text) {

    return !text.match(/([a-z]).*\1/i);
}
//console.log(isIsogram('Machine'))


function purchase(age, price) {

    if (age < 13) {

        return undefined;

    } else if ((age >= 13 && age <= 21) || age >= 65) {

        return (price * 0.8).toFixed(2)

    } else {

        return price.toFixed(2)
    }

}

//console.log(purchase(21,109.4356))

// const itemsArr = [
//     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
//     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
//     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
//     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
//     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// ];

function findHotCategories(items) {

    hotCategoriesArr = []

    for (let i = 0; i < items.length; i++) {

        if (items[i].stocks === 0) {

            hotCategoriesArr.push(items[i].category);

        } else {

            continue
        }
    }

    for (let i = 0; i < hotCategoriesArr.length; i++) {

        for (let a = i + 1; a < hotCategoriesArr.length; a++) {

            if (hotCategoriesArr[i] === hotCategoriesArr[a]) {
                hotCategoriesArr.splice(a, 1);
            } else {
                continue;
            }
        }
    }

    return hotCategoriesArr;
}

//  console.log(findHotCategories(itemsArr))


// const candidateAArr = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
// const candidateBArr = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

function findFlyingVoters(candidateA, candidateB) {
    
    voterArr = []

    for (let a = 0; a < candidateA.length; a++) {

        for (let b = 0; b < candidateB.length; b++) {

            if (candidateA[a] === candidateB[b]) {

                voterArr.push(candidateA[a]);

            } else {

                continue
            }
        }
    }

    return voterArr
    
}
// console.log(findFlyingVoters(candidateAArr, candidateBArr))

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};